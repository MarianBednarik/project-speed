import React from 'react';
import styled, { withTheme } from 'styled-components';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Image from 'react-graceful-image';
import spankbang from './favicons/spankbang.jpg';

const PlayButton = styled.span`
  opacity: 0;
  transition-duration: 0.2s;
  position: absolute;
  bottom: 60px;
  left: 132.7px;
  color: ${props => props.theme.accentColor};
  font-size: 3rem;
  text-shadow: 0 2px 10px rgba(0,0,0,0.10), 0 5px 14px rgba(0,0,0,0.10);
`;

const Favicon = styled.img`
  position: absolute;
  top: 8px;
  left: 8px;
  width: 16px;
  height: 16px;
  box-shadow: 0 1px 6px rgba(0,0,0,0.1), 0 3px 10px rgba(0,0,0,0.1);
`;

const Card = styled.div`
  background-color: ${props => props.theme.contentColor};
  transition-duration: 0.2s;
  width: 300px;
  box-shadow: 0 1px 6px ${props => (props.accented ? props.theme.accentColor : 'rgba(0,0,0,0.08)')}, 0 3px 10px ${props => (props.accented ? props.theme.accentColor : 'rgba(0,0,0,0.08)')};
  border-radius: 6px;
  overflow: hidden;
  cursor: pointer;
  &:hover {
    box-shadow: 0 2px 10px rgba(0,0,0,0.10), 0 5px 14px rgba(0,0,0,0.10);
    transform: scale(1.01);
    img {
      transition-duration: 0.2s;
      filter: brightness(1.2) contrast(0.8);
    }
    ${PlayButton} {
      transition-duration: 0.2s;
      opacity: 0.9;
    }
  }
`;

const ImageWrapper = styled.div`
  position: relative;
  height: 170px;
  overflow: hidden;
`;

const Title = styled.p`
  line-height: 32px;
  padding: 0 0.25rem;
  margin: 0;
  font-size: 14px;
  color: ${props => props.theme.textColor};
  text-align: center;
  overflow: hidden; 
  white-space: nowrap;
  text-overflow: ellipsis;
`;

const TagWrapper = styled.div`
  text-align: center;
  padding: 0rem 0.25rem;
  font-size: 12px;
  line-height: 24px;
  background-color: ${props => props.theme.secondaryColor};
  color: ${props => props.theme.secondaryTextColor};
  overflow: hidden; 
  white-space: nowrap;
  text-overflow: ellipsis;
`;

const VideoPreview = ({
  video, id, noLazyLoad, theme,
}) => {
  const link = `/v/${id}`;
  const tags = video.tags.length > 0 ? video.tags.join(' · ') : '　';
  return (
    <Link to={link} style={{ textDecoration: 'none', display: 'block' }}>
      <Card >
        <ImageWrapper>
          <Image
            src={video.thumbnail}
            width="300"
            height="170"
            alt={video.title}
            placeholderColor={theme.secondaryColor}
            noLazyLoad={noLazyLoad}
          />
          <PlayButton>▶</PlayButton>
          <Favicon src={spankbang} alt={video.provider} />
        </ImageWrapper>
        <Title title={video.title}>{video.title}</Title>
        <TagWrapper title={tags}>
          {tags}
        </TagWrapper>
      </Card>
    </Link>
  );
};

VideoPreview.propTypes = {
  video: PropTypes.shape({
    thumbnail: PropTypes.string,
    title: PropTypes.string,
    time: PropTypes.number,
    tags: PropTypes.array,
    provider: PropTypes.string,
  }),
};

VideoPreview.defaultProps = {
  video: {
    thumbnail: null,
    title: '　',
    time: 0,
    tags: [],
    provider: '　',
  },
};

export default withTheme(VideoPreview);
