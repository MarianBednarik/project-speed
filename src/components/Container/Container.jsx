import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import media from '/services/style-utils';

const Wrapper = styled.div`
  display: grid;
  padding: 0 16px;
  margin: 0 auto;
  width: 100%;
  ${props => props.fluid && css`
    max-width: 1920px;
    padding: 0 8px;
  `}
  ${props => !props.fluid && css`
    ${media.sm`max-width: 540px;`}
    ${media.md`max-width: 720px;`}
    ${media.lg`max-width: 960px;`}
    ${media.xl`max-width: 1300px;`}
  `}
`;

const Container = ({
  ...props, children,
}) => (
  <Wrapper {...props} >
    {children}
  </Wrapper>
);

Container.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  fluid: PropTypes.bool,
};

Container.defaultProps = {
  className: null,
  fluid: false,
};

export default Container;
