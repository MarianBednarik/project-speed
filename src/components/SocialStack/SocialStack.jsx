import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { Facebook, Twitter, Instagram } from '/components/Icon/Icon';

const Wrapper = styled.div`
  display: grid;
  grid-auto-columns: 24px;
  grid-auto-flow: column;
  grid-column-gap: 16px;
  align-items: center;
  a {
    text-decoration: none;
    svg {
      display: block;
      transition-duration: 0.2s;
      &:hover {
        stroke: ${props => props.colors.hover}
      }
      &:active {
        stroke: ${props => props.colors.active}
      }
    }
  }
`;

const SocialStack = ({
  className, colorMain, colorHover, colorActive,
}) => {
  const colors = {
    main: colorMain,
    hover: colorHover,
    active: colorActive,
  };
  return (
    <Wrapper className={className} colors={colors}>
      <a href="https://facebook.com">
        <Facebook strokeColor={colors.main} />
      </a>
      <a href="https://twitter.com">
        <Twitter strokeColor={colors.main} />
      </a>
      <a href="https://instagram.com">
        <Instagram strokeColor={colors.main} />
      </a>
    </Wrapper>
  );
};

SocialStack.propTypes = {
  className: PropTypes.string.isRequired,
  colorMain: PropTypes.string,
  colorHover: PropTypes.string,
  colorActive: PropTypes.string,
};

SocialStack.defaultProps = {
  colorMain: '#546E7A',
  colorHover: '#37474F',
  colorActive: '#00BFA5',
};

export default SocialStack;
