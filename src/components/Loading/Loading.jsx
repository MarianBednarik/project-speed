import React from 'react';
import styled, { keyframes, css } from 'styled-components';

const Spinner = styled.div`
  margin: 100px auto 0;
  width: 100px;
  text-align: center;
`;

const Bounce = keyframes`
  0%, 80%, 100% { 
    transform: scale(0);
  } 40% { 
    transform: scale(1.0);
  }
`;

const Circle = styled.div`
  width: 12px;
  height: 12px;
  background-color: #EEE;
  margin: 0 0.1rem;
  border-radius: 100%;
  display: inline-block;
  -webkit-animation: ${Bounce} 1.4s infinite ease-in-out both;
  animation: ${Bounce} 1.4s infinite ease-in-out both;
  ${props => props.first && css`
    animation-delay: -0.32s;
  `}
  ${props => props.second && css`
    animation-delay: -0.16s;
  `}
`;

export default () => (
  <Spinner>
    <Circle first />
    <Circle second />
    <Circle />
  </Spinner>
);
