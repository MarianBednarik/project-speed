import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import VideoPreview from '/components/VideoPreview/VideoPreview';
import media from '/services/style-utils';

const VideoWrapper = styled.div`
  display: grid;
  width: 100%;
  margin: 0 auto;
  grid-auto-flow: row dense;
  grid-template-columns: repeat(auto-fill, minmax(200px, 300px));
  grid-gap: 1rem;
  justify-content: center;
  position: relative;
`;

const Overlay = styled.h1`
  color: ${props => props.theme.textColor};
  text-shadow: 0px 0px 50px black;
  position: absolute;
  justify-self: center;
  align-self: center;
`;

const VideoGrid = ({
  ...props, videos, noLazyLoad, total,
}) => {
  const videoPreviews = [];
  const videosLength = videos.length;

  for (let i = 0; i < total; i += 1) {
    const video = videosLength > 0 ? videos[i] : undefined;
    videoPreviews.push(<VideoPreview
      video={video ? video._source : undefined}
      id={video ? video._id : 'random'}
      key={`id${i}`}
      noLazyLoad={noLazyLoad}
    />);
  }

  return (
    <VideoWrapper {...props}>
      {videoPreviews}
      {(videosLength === 0 && total > 0) && <Overlay>Loading...</Overlay>}
    </VideoWrapper>
  );
};

VideoGrid.propTypes = {
  videos: PropTypes.array,
  noLazyLoad: PropTypes.bool,
};

VideoGrid.defaultProps = {
  videos: [],
  noLazyLoad: false,
};

export default VideoGrid;
