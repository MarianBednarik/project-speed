import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

import { NavLink } from 'react-router-dom';

const Wrapper = styled.div`
  padding-top: 2rem;
  display: grid;
  grid-template-columns: repeat(2, minmax(0px, 300px));
  grid-template-areas: "prev next";
  align-content: center;
  justify-content: center;
  justify-items: center;
`;

const StyledLink = styled(NavLink)`
  font-size: 1.25rem;
  padding: 0.5rem 1rem;
  border-radius: 6px;
  color: ${props => props.theme.textColor};
  text-decoration: none;
  transition-duration: 0.2s;
  background-color: ${props => props.theme.contentColor};
  &:hover {
    color: ${props => props.theme.accentColor};
    i {
      border: solid ${props => props.theme.accentColor}
      border-width: 0 3px 3px 0;
    }
  }
`;

const Arrow = styled.i`
  transition-duration: 0.2s;
  border: solid ${props => props.theme.textColor};
  border-width: 0 3px 3px 0;
  display: inline-block;
  padding: 4px;
  ${props => props.left && css`
    transform: rotate(135deg) translateY(3px);
  `}
  ${props => props.right && css`
    transform: rotate(-45deg) translateY(-2px);
  `}
`;

const Paginator = ({
  ...props, currentPage, lastPage, searchTerm,
}) => (
  <Wrapper {...props}>
    {currentPage > 0 && <StyledLink to={searchTerm ? `/s/${searchTerm}/${currentPage - 1}` : `/new/${currentPage - 1}`} style={{ gridArea: 'prev' }} onClick={() => { window.scrollTo(0, 0); }}><Arrow left /> Previous</StyledLink>}
    {currentPage !== lastPage && <StyledLink to={searchTerm ? `/s/${searchTerm}/${currentPage + 1}` : `/new/${currentPage + 1}`} style={{ gridArea: 'next' }} onClick={() => { window.scrollTo(0, 0); }}>Next <Arrow right /></StyledLink>}
  </Wrapper>
);

Paginator.propTypes = {

};

Paginator.defaultProps = {

};

export default Paginator;
