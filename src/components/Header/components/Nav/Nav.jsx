import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled, { withTheme } from 'styled-components';
import media from '/services/style-utils';
import { Menu } from '/components/Icon/Icon';
import { NavLink } from 'react-router-dom';

const items = {
  new: {
    name: 'New',
    link: '/new',
    extra: '/0',
  },
  categories: {
    name: 'Categories',
    link: '/categories',
  },
  random: {
    name: 'Random',
    link: '/v/random',
  },
};

const Wrapper = styled.div`
  display: grid;
  grid-auto-flow: row;
  grid-gap: 8px;
  ${media.sm`
    grid-auto-flow: column;
  `}
  grid-column-gap: 16px;
`;

const StyledLink = styled(NavLink)`
  text-decoration: none;
  padding: 0.5rem 0;
  transition-duration: 0.2s;
  color: ${props => props.theme.secondaryTextColor};
  width: fit-content;
  justify-self: end;
  &:hover{
    color: ${props => props.theme.accentColor};
  }
`;

const Nav = ({
  style, className, navToggle, theme,
}) => (
  <Wrapper style={style} className={className}>
    {Object.keys(items).map((item, i) => (
      <StyledLink
        to={`${items[item].link}${items[item].extra ? items[item].extra : ''}`}
        style={items[item].name === 'Random' ? { backgroundColor: theme.contentColor, padding: '0.5rem 1rem', borderRadius: '4px' } : null}
        activeStyle={items[item].name !== 'Random' ? { color: theme.accentColor, textDecoration: 'underline' } : { backgroundColor: theme.accentColor, color: theme.primaryColor }}
        onClick={() => { navToggle('nav'); }}
      >
        {items[item].name}
      </StyledLink>
    ))}
  </Wrapper>
);

export default withTheme(Nav);
