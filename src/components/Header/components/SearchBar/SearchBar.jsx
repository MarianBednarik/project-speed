import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import media from '/services/style-utils';
import styled, { css } from 'styled-components';
import { Search, Close } from '/components/Icon/Icon';

const Wrapper = styled.div`
  display:grid;
  justify-items: right;
  grid-template-columns: 1fr 24px;
  grid-template-areas: "input icon";
  svg#search_icon {
    grid-area: icon;
    stroke: ${props => props.theme.secondaryTextColor};
    margin: 20px 0;
    ${media.sm`margin: 38px 0`}
    &:hover {
      stroke: ${props => props.theme.accentColor};
    }
  }
`;

const InputWrapper = styled.div`
  position: relative;
  grid-area: input;
`;

const InputField = styled.input`
  transition-duration: 0.2s;
  color: #fff;
  max-height: 0px;
  border-radius: 4px;
  font-size: 1rem;
  -webkit-appearance: none;
  box-shadow: 0 1px 6px rgba(0,0,0,0.08), 0 3px 10px rgba(0,0,0,0.08);
  margin-right: 16px;
  width: 0;
  padding: 0;
  border: none;
  &:focus {
    outline: none;
  }
  ${props => props.searchOpened && css`
    color: #212121;
    padding: 0.75rem 2.25rem 0.75rem 0.75rem;
    width: calc(100% - 16px);
    max-height: 64px;
  `}
`;

const ClearSearch = styled(Close)`
  position: absolute;
  right: 24px;
  bottom: calc( 50% - 12px);
  width: 0px;
  transition-duration: 0.2s;
  cursor: pointer;
  ${props => props.searchOpened && css`
    width: 24px;
  `}
`;

class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.inputRef = React.createRef();
  }

  handleChange(event) {
    this.setState({ text: event.target.value });
  }

  handleSubmit(event) {
    if (event) { event.preventDefault(); }
    if (this.state.text.trim() !== '') {
      this.props.history.push(`/s/${this.state.text.trim()}/0`);
      this.inputSearch.blur();
    }
  }

  handleClick(event) {
    if (!this.props.searchOpened) {
      this.props.clickHandler('search');
      this.inputSearch.focus();
    } else {
      if (event !== 'search') {
        this.props.clickHandler('search');
      } else {
        this.handleSubmit(undefined);
      }
      this.inputSearch.blur();
    }
  }

  render() {
    const {
      style, className, searchOpened,
    } = this.props;
    return (
      <Wrapper style={style} className={className}>
        <form onSubmit={this.handleSubmit} style={{ width: '100%', alignSelf: 'center', textAlign: 'right' }}>
          <InputWrapper>
            <InputField
              type="text"
              innerRef={(input) => { this.inputSearch = input; }}
              searchOpened={searchOpened}
              value={this.state.text}
              onChange={this.handleChange}
            />
            <ClearSearch searchOpened={searchOpened} clickHandler={this.handleClick} />
          </InputWrapper>
        </form>
        <Search clickHandler={this.handleClick} style={{ cursor: 'pointer' }} />
      </Wrapper>
    );
  }
}

export default withRouter(SearchBar);
