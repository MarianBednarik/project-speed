import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled, { css, withTheme } from 'styled-components';
import media from '/services/style-utils';
import Container from '/components/Container/Container';
import Logo from '/components/Logo/Logo';
import { Menu } from '/components/Icon/Icon';
import SearchBar from './components/SearchBar/SearchBar';
import Nav from './components/Nav/Nav';

const StyledContainer = styled(Container)`
  background-color: transparent;
  align-items: center;
`;

const Wrapper = styled.div`
  display: grid;
  align-items: center;
  grid-auto-flow: column;
  grid-auto-columns: auto;
  grid-auto-rows: auto;
  grid-column-gap: 16px;
  grid-template-columns: auto 1fr auto auto;
  grid-template-areas:
      "logo search search toggle"
      "nav nav nav nav";

  ${props => !props.searchOpened && css`
    ${media.sm`
      grid-template-columns: auto 1fr auto auto;
      grid-template-areas: "logo search search nav";
      grid-column-gap: 32px;
    `}
  `}
  
  ${props => props.searchOpened && css`
    a#logo {
      display: none;
    }
    ${media.sm`
      grid-column-gap: 32px;
      grid-template-areas: "logo search search nav";
      a#logo {
        display: block;
      }
    `}
  `}

  svg#menu_icon {
    display: block;
    grid-area: toggle;
    ${media.sm`display: none;`}
    stroke: ${props => props.theme.secondaryTextColor};
    &:hover {
      stroke: ${props => props.theme.accentColor};
    }
  }
`;

const ResposiveNav = styled(Nav)`
  grid-area: nav;
  max-height: 0px;
  overflow: hidden;
  background-color: ${props => props.theme.secondaryColor};
  transition-duration: 0.2s;
  text-align: right;
  margin: 0 -16px;
  padding: 0;
  ${media.sm`
    max-height: 100px;
    background-color: transparent;
    margin: 0;
    padding: 0;
  `}
  ${props => props.open && css`
    max-height: 152px;
    padding: 16px 16px;
  `}
`;

class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      navOpened: false,
      searchOpened: false,
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle(target) {
    switch (target) {
      case 'nav':
        this.setState({ navOpened: !this.state.navOpened });
        break;
      case 'search':
        this.setState({ searchOpened: !this.state.searchOpened });
        break;
      default:
        break;
    }
  }

  render() {
    return (
      <StyledContainer className={this.props.className}>
        <Wrapper searchOpened={this.state.searchOpened}>
          <Logo size="normal" style={{ gridArea: 'logo' }} />
          <SearchBar
            style={{ gridArea: 'search' }}
            clickHandler={this.toggle}
            searchOpened={this.state.searchOpened}
          />
          <Menu style={{ cursor: 'pointer' }} clickHandler={this.toggle} />
          <ResposiveNav open={this.state.navOpened} navToggle={this.toggle} />
        </Wrapper>
      </StyledContainer>
    );
  }
}

Header.propTypes = {
  className: PropTypes.string,
};

Header.defaultProps = {
  className: null,
};

export default withTheme(Header);
