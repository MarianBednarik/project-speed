import React from 'react';
import PropTypes from 'prop-types';
import styled, { withTheme } from 'styled-components';
import media from '/services/style-utils';

const Wrapper = styled.h2`
  margin: 2rem 0 1.5rem 0;
  ${media.sm`margin: 3rem 0 1.5rem 0;`}
  color: ${props => props.theme.textColor};
  position: relative;
  width: fit-content;
  &:before {
    position: absolute;
    box-shadow: 0 1px 6px rgba(0,0,0,0.08), 0 3px 10px rgba(0,0,0,0.08);
    content: '';
    top: -16px;
    left: -32px;
    width: calc(100% + 64px);
    height: 4px;
    background-color: ${props => props.accent};
  }
  &:after {
    position: absolute;
    box-shadow: 0 1px 6px rgba(0,0,0,0.08), 0 3px 10px rgba(0,0,0,0.08);
    content: '';
    bottom: 0;
    right: -16px;
    width: 4px;
    height: calc(100% + 32px);
    background-color: ${props => props.accent};
  }
`;

const Title = ({
  ...props, children, theme,
}) => (
  <Wrapper {...props} accent={theme.accentColor}>
    {children}
  </Wrapper>
);

Title.propTypes = {
  children: PropTypes.node,
};

Title.defaultProps = {
  children: undefined,
};

export default withTheme(Title);
