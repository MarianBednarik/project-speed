import React from 'react';
import styled, { css } from 'styled-components';
import PropTypes from 'prop-types';

const StyledButton = styled.button`
  display: inline-block;
  min-width: 64px;
  width: fit-content;
  height: 36px;
  line-height: 36px;
  padding: 0 16px;
  border-radius: 20px;
  font-size: 14px;
  letter-spacing: .035em;
  color: #212121;
  transition-duration: 0.15s;
  cursor: pointer;
  font-weight: 500;
  box-shadow: 0 4px 6px rgba(50,50,93,.11), 0 1px 3px rgba(0,0,0,.08);
  border: none;
  outline: none;
  text-transform: uppercase;
  transition: all .2s ease;
  background-color: #fff;

  &:hover{
    transform: translateY(-1px);
    box-shadow: 0 4px 9px rgba(50,50,93,.16), 0 1px 4px rgba(0,0,0,.12);
  }

  &:active{
    transform: translateY(0px);
    box-shadow: 0 4px 6px rgba(50,50,93,.11), 0 1px 3px rgba(0,0,0,.08);
  }

  /* TEXT */
  ${props => props.text && css`
    box-shadow: none;
    color: #00695C;
    &:hover{
      transform: none;
      box-shadow: none;
      filter: brightness(110%);
    }
  `}

  /* OUTLINED */
  ${props => props.outlined && css`
    color: #00695C;
    line-height: 32px;
    border: 2px solid #00BFA5;
    box-shadow: 0 4px 6px rgba(50,50,93,.11), 0 1px 3px rgba(0,0,0,.08);
    &:hover{
      filter: brightness(110%);
    }
  `}

  /* CONTAINED */
  ${props => props.contained && css`
    background-image: linear-gradient(to right, #1DE9B6, #00BFA5);
    color: #004D40;
    &:hover{
      filter: brightness(110%);
    }
    &:active{
      filter: brightness(100%);
    }
  `}
`;

const Button = ({
  children, style, text, outlined, contained, toggle,
}) => (
  <StyledButton
    style={style}
    text={text}
    outlined={outlined}
    contained={contained}
    toggle={toggle}
  >
    {children}
  </StyledButton>
);

Button.propTypes = {
  children: PropTypes.node.isRequired,
  text: PropTypes.bool,
  outlined: PropTypes.bool,
  contained: PropTypes.bool,
  toggle: PropTypes.bool,
};

Button.defaultProps = {
  text: false,
  outlined: false,
  contained: false,
  toggle: false,
};

export default Button;
