import React, { Component } from 'react';
import styled, { css } from 'styled-components';
import adVertical from './ad_vertical.jpg';
import adHorizontal from './ad_horizontal.jpg';

const Image = styled.img`
  width: 100%;
  height: 100%;
  background-color: #eee;
  border-radius: 6px;
  color: #616161;
`;

const Wrapper = styled.div`
  display: grid;
  width: 100%;
  justify-content: center;
  align-content: center;
  grid-auto-flow: row;
  grid-gap: 1rem;
  text-align: center;
  ${props => props.mobile && css`
    grid-auto-flow: column;
  `}
  a {
    color: ${props => props.theme.textColor};
  }
`;

class Ad extends Component {
  componentDidMount() {
    let script;
    switch (this.props.type) {
      case 'promoted-videos-homepage':
        script = document.createElement('script');
        script.type = 'text/javascript';
        script.setAttribute('data-idzone', '3060066');
        script.src = 'https://ads.exosrv.com/nativeads.js';
        document.getElementById('promoted-videos-homepage').appendChild(script);
        break;
      default:
        break;
    }
  }

  render() {
    switch (this.props.type) {
      case 'big-banner':
        return (
          <Wrapper>
            <iframe
              {...this.props}
              title="ExoClick Ad Banner"
              src="//ads.exosrv.com/iframe.php?idzone=3059888&size=900x250"
              width="900"
              height="250"
              scrolling="no"
              marginWidth="0"
              marginHeight="0"
              frameBorder="0"
            />
          </Wrapper>
        );
      case 'small-banner':
        return (
          <Wrapper>
            <iframe
              {...this.props}
              title="promoted"
              src="//ads.exosrv.com/iframe.php?idzone=3062546&size=728x90"
              width="728"
              height="90"
              scrolling="no"
              marginWidth="0"
              marginHeight="0"
              frameBorder="0"
            />
          </Wrapper>
        );
      case 'mobile-small-banner':
        return (
          <Wrapper>
            <iframe
              title="mobile-default"
              src="//ads.exosrv.com/iframe.php?idzone=3063040&size=300x100"
              width="300"
              height="100"
              scrolling="no"
              marginWidth="0"
              marginHeight="0"
              frameBorder="0"
            />
          </Wrapper>
        );
      case 'mobile-big-banner':
        return (
          <Wrapper>
            <iframe
              title="mobile-default"
              src="//ads.exosrv.com/iframe.php?idzone=3063126&size=300x250"
              width="300"
              height="250"
              scrolling="no"
              marginWidth="0"
              marginHeight="0"
              frameBorder="0"
            />
          </Wrapper>
        );
      case 'square':
        return (
          <Wrapper {...this.props}>
            <iframe
              title={`${this.props.type}-1`}
              src="//ads.exosrv.com/iframe.php?idzone=3059976&size=300x250"
              width="300"
              height="250"
              scrolling="no"
              marginWidth="0"
              marginHeight="0"
              frameBorder="0"
            />
          </Wrapper>
        );
      case 'dual-square':
        return (
          <Wrapper mobile={this.props.mobile} {...this.props}>
            <iframe
              title={`${this.props.type}-1`}
              src="//ads.exosrv.com/iframe.php?idzone=3059976&size=300x250"
              width="300"
              height="250"
              scrolling="no"
              marginWidth="0"
              marginHeight="0"
              frameBorder="0"
            />
            <iframe
              title={`${this.props.type}-2`}
              src="//ads.exosrv.com/iframe.php?idzone=3059984&size=300x250"
              width="300"
              height="250"
              scrolling="no"
              marginWidth="0"
              marginHeight="0"
              frameBorder="0"
            />
            {/* <a href="https://www.exoclick.com/?login=Qwenty" target="_blank" rel="noopener noreferrer">Ads by <b>ExoClick</b></a> */}
          </Wrapper>
        );
      case 'local':
        return (
          <a href="https://m.do.co/c/8bfa077778a2" target="_blank" rel="noopener noreferrer" {...this.props}>
            <Image src={window.innerWidth > 992 ? adVertical : adHorizontal} />
          </a>
        );
      default:
        return (
          <Wrapper id={this.props.type} {...this.props} />
        );
    }
  }
}

export default Ad;
