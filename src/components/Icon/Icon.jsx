import React from 'react';
import PropTypes from 'prop-types';

const Facebook = ({ strokeColor }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    stroke={strokeColor}
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
  >
    <path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z" />
  </svg>
);

Facebook.propTypes = {
  strokeColor: PropTypes.string,
};

Facebook.defaultProps = {
  strokeColor: '#546E7A',
};

const Instagram = ({ strokeColor }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    stroke={strokeColor}
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
  >
    <rect x="2" y="2" width="20" height="20" rx="5" ry="5" />
    <path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z" />
    <line x1="17.5" y1="6.5" x2="17.5" y2="6.5" />
  </svg>
);

Instagram.propTypes = {
  strokeColor: PropTypes.string,
};

Instagram.defaultProps = {
  strokeColor: '#546E7A',
};

const Twitter = ({ strokeColor }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    stroke={strokeColor}
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
  >
    <path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z" />
  </svg>
);

Twitter.propTypes = {
  strokeColor: PropTypes.string,
};

Twitter.defaultProps = {
  strokeColor: '#546E7A',
};

const Search = ({ strokeColor, style, clickHandler }) => (
  <svg
    id="search_icon"
    style={style}
    onClick={() => clickHandler('search')}
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    stroke={strokeColor}
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
  >
    <circle cx="11" cy="11" r="8" />
    <line x1="21" y1="21" x2="16.65" y2="16.65" />
  </svg>
);

Search.propTypes = {
  strokeColor: PropTypes.string,
};

Search.defaultProps = {
  strokeColor: '#546E7A',
};

const Menu = ({ strokeColor, clickHandler, style }) => (
  <svg
    id="menu_icon"
    onClick={() => clickHandler('nav')}
    style={style}
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    stroke={strokeColor}
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
  >
    <line x1="3" y1="12" x2="21" y2="12" />
    <line x1="3" y1="6" x2="21" y2="6" />
    <line x1="3" y1="18" x2="21" y2="18" />
  </svg>
);

Menu.propTypes = {
  strokeColor: PropTypes.string.isRequired,
};

const Close = ({
  strokeColor, clickHandler, style, className,
}) => (
  <svg
    id="close_icon"
    onClick={() => clickHandler()}
    style={style}
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    stroke={strokeColor}
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
  >
    <line x1="18" y1="6" x2="6" y2="18" />
    <line x1="6" y1="6" x2="18" y2="18" />
  </svg>
);

Close.propTypes = {
  strokeColor: PropTypes.string,
};

Close.defaultProps = {
  strokeColor: '#546E7A',
};

const Mail = ({
  strokeColor, clickHandler, style, className,
}) => (
  <svg
    id="close_icon"
    onClick={() => clickHandler()}
    style={style}
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    stroke={strokeColor}
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
  >
    <path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z" />
    <polyline points="22,6 12,13 2,6" />
  </svg>
);

Mail.propTypes = {
  strokeColor: PropTypes.string,
};

Mail.defaultProps = {
  strokeColor: '#546E7A',
};

export { Facebook, Instagram, Twitter, Search, Menu, Close, Mail };
