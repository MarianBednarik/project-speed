import React from 'react';
import styled, { css, withTheme } from 'styled-components';
import Container from '/components/Container/Container';

import { NavLink } from 'react-router-dom';
import media from '/services/style-utils';
import { Mail, Twitter } from '/components/Icon/Icon';

const StyledFooter = styled.footer`
  position: absolute;
  width: 100%;
  min-height: 160px;
  bottom: 0px;
  color: ${props => props.theme.textColor};
  background-color: ${props => props.theme.contentColor};
  text-align: center;
`;

const StyledContainer = styled(Container)`
  display: grid;
  grid-auto-columns: minmax(1fr, 300px);
  grid-auto-flow: column;
  grid-template-areas:"contact contact"
                      "links content";
  ${media.sm`
    grid-template-areas: "links content"
                          "contact content";
  `}
  ${media.md`
    grid-template-areas: "links content contact";
  `}
`;

const Wrapper = styled.div`
  display: grid;
  grid-auto-flow: row;
  grid-template-rows: 60px;
  grid-auto-rows: auto;
`;

const Title = styled.h3`
  position: relative;
  height: fit-content;
  margin: 1rem;
  &:after {
    content: '';
    position: absolute;
    background-color: ${props => props.theme.accentColor};
    bottom: -8px;
    left: calc(50% - 50px);
    width: 100px;
    height: 2px;
  }
`;

const List = styled.ul`
  margin: 0 0 1rem 0;
  padding: 0;
  display: grid;
  grid-gap: 0.5rem;
  justify-items: center;
`;

const ListItem = styled.li`
  list-style-type: none;
  display: grid;
  ${props => props.withIcon && css`
    grid-template-columns: 24px auto;
    margin-left: -8px;
  `}
  grid-auto-flow: column;
  grid-gap: 1rem;
  a {
    color: ${props => props.theme.textColor};
    width: fit-content;
    text-align: center;
    margin: 0;
    line-height: 24px;
  }
  svg {
    stroke: ${props => props.theme.secondaryTextColor};
  }
`;

const Footer = ({ theme }) => (
  <StyledFooter>
    <StyledContainer>
      <Wrapper style={{ gridArea: 'links' }}>
        <Title>LINKS</Title>
        <List>
          <ListItem>
            <NavLink to="/info">About Us</NavLink>
          </ListItem>
          <ListItem>
            <NavLink to="/info">Legal</NavLink>
          </ListItem>
        </List>
      </Wrapper>
      <Wrapper style={{ gridArea: 'content', alignItems: 'start' }}>
        <Title>CONTENT</Title>
        <List>
          <ListItem>
            <a href="https://spankbang.com">Spankbang</a>
          </ListItem>
          <ListItem>
            <a style={{ color: theme.secondaryTextColor }}>More coming soon</a>
          </ListItem>
        </List>
      </Wrapper>
      <Wrapper style={{ gridArea: 'contact' }}>
        <Title>CONTACT</Title>
        <List>
          <ListItem withIcon>
            <Mail />
            <a href={`mailto:contact@${theme.name.toLowerCase()}.com`}>{`contact@${theme.name.toLowerCase()}.com`}</a>
          </ListItem>
          <ListItem withIcon>
            <Twitter />
            <a href={`http://twitter.com/${theme.name}`} target="_blank" rel="noopener noreferrer">{theme.name}</a>
          </ListItem>
        </List>
      </Wrapper>
    </StyledContainer>
  </StyledFooter>
);

export default withTheme(Footer);
