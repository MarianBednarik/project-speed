import React from 'react';
import styled from 'styled-components';

import links from '/services/Providers';

const Card = styled.div`
  width: 100%;
  background-color: ${props => props.theme.contentColor};
  height: fit-content;
  border-radius: 6px;
  overflow: hidden;
  box-shadow: 0 2px 10px rgba(0,0,0,0.10), 0 5px 14px rgba(0,0,0,0.10);
`;

const VideoContainer = styled.div`
  position: relative;
  overflow: hidden;
  padding-top: 56.25%;
  &:after {
    content: '';
    position: absolute;
    width: 100%;
    height: 4px;
    background-color: ${props => props.theme.accentColor};
    bottom: 0;
  }
`;

const VideoEmbed = styled.iframe`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  border: 0;
`;

const Title = styled.h3`
  margin: 1rem 2rem 1rem 2rem;
  color: ${props => props.theme.textColor};
`;

const Description = styled.p`
  padding: 1rem 2rem 1rem 2rem;
  background-color: ${props => props.theme.secondaryColor};
  color: ${props => props.theme.secondaryTextColor};
  margin-bottom: 0;
  display: grid;
  grid-auto-flow: column;
  grid-gap: 16px;
  font-size: 0.9rem;
`;

const secToTime = (secs) => {
  let minutes = Math.floor(secs / 60);
  secs %= 60;
  const hours = Math.floor(minutes / 60);
  minutes %= 60;
  return `${hours ? `${hours}h ` : ''}${minutes ? `${minutes}m ` : ''}${secs ? `${secs}s ` : ''}`;
};

export default ({ style, video }) => {
  let link;
  let description;
  if (video) {
    link = links[video.provider].replace('{id}', video.id);
    description = `${video.provider} | ${secToTime(video.time)} | ${video.tags.join(' · ')}`;
  }

  return (
    <Card style={style}>
      <VideoContainer>
        <VideoEmbed src={video ? link : ''} frameBorder="0" scrolling="no" allowFullScreen />
      </VideoContainer>
      <Title>{video ? video.title : ''}</Title>
      <Description>{video ? description : ''}</Description>
    </Card>
  );
};
