import React from 'react';
import PropTypes from 'prop-types';
import styled, { withTheme } from 'styled-components';
import media from '/services/style-utils';
import { Link } from 'react-router-dom';

const sizes = {
  large: 128,
  normal: 64,
  small: 32,
};

const StyledLink = styled(Link)`
  text-decoration: none;
  color: ${props => props.theme.textColor};
  h1 {
    font-family: 'Pacifico';
    font-weight: 300;
    margin: 0;
    &:after{
      font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
      content: " BETA";
      color: ${props => props.theme.accentColor};
      font-size: 0.8rem;
    }
  }
`;

const Logo = ({
  size, className, style, theme,
}) => (
  <StyledLink to="/" style={style} id="logo">
    <h1>
      {theme.name}
    </h1>
  </StyledLink>
);

Logo.propTypes = {
  size: PropTypes.string,
  className: PropTypes.string,
};

Logo.defaultProps = {
  size: 'normal',
  className: null,
};

export default withTheme(Logo);
