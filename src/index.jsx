import React from 'react';
import { render } from 'react-dom';
import { ThemeProvider } from 'styled-components';
import { BrowserRouter as Router } from 'react-router-dom';
import App from './App';

const themes = {
  light: {
    accentColor: '#FEC109',
    textColor: '#212121',
    secondaryTextColor: '#616161',
    primaryColor: '#FAFAFA',
    secondaryColor: '#EEEEEE',
    contentColor: '#FFFFFF',
    name: 'Reporned',
  },
  dark: {
    accentColor: '#F50057',
    textColor: '#FAFAFA',
    secondaryTextColor: '#BDBDBD',
    primaryColor: '#212121',
    secondaryColor: '#424242',
    contentColor: '#616161',
    name: 'Reporned',
  },
  night: {
    accentColor: '#1DE9B6',
    textColor: '#ECEFF1',
    secondaryTextColor: '#90A4AE',
    primaryColor: '#263238',
    secondaryColor: '#37474F',
    contentColor: '#455A64',
    name: 'Reporned',
  },
};

render(
  <Router>
    <ThemeProvider theme={themes[process.env.THEME ? process.env.THEME : 'light']}>
      <App />
    </ThemeProvider>
  </Router>,
  document.getElementById('app'),
);
