import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import Header from '/components/Header/Header';
import Homepage from '/scenes/Homepage/Homepage';
import Watch from '/scenes/Watch/Watch';
import Search from '/scenes/Search/Search';
import New from '/scenes/New/New';
import Categories from '/scenes/Categories/Categories';
import Info from '/scenes/Info/Info';
import Footer from '/components/Footer/Footer';
import { withTheme } from '../node_modules/styled-components';

class App extends Component {
  componentDidMount() {
    document.body.style = `background-color: ${this.props.theme.primaryColor};`;
  }

  render() {
    return (
      <React.Fragment>
        <Header />
        <Route exact path="/" component={Homepage} />
        <Route exact path="/v/:id" component={Watch} />
        <Route exact path="/s/:term/:page" component={Search} />
        <Route exact path="/new/:page" component={New} />
        <Route exact path="/categories" component={Categories} />
        <Route exact path="/info" component={Info} />
        <Footer />
      </React.Fragment>
    );
  }
}

export default withTheme(App);
