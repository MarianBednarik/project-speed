import axios from 'axios';

export default class Client {
  constructor(index) {
    this.host = undefined;
    this.index = index;
  }

  get(query) {
    return axios({
      method: 'post',
      url: `https://${this.host}/${this.index}/_search`,
      auth: undefined,
      params: {
        source: JSON.stringify(query),
        source_content_type: 'application/json',
      },
    });
  }
}

export const client = new Client('videos/video');
