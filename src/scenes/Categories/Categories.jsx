import React from 'react';
import styled, { withTheme } from 'styled-components';
import { NavLink } from 'react-router-dom';
import media from '/services/style-utils';
import Container from '/components/Container/Container';
import Media from 'react-media';
import Ad from '/components/Ad/Ad';

const categories = ['Amateur', 'Anal', 'Asian', 'Babe', 'BBW', 'Big Ass', 'Big Dick', 'Big Tits', 'Blonde', 'Blowjob', 'Bondage', 'Brunette', 'Cam', 'Compilation', 'Creampie', 'Cumshot', 'Deep Throat', 'DP', 'Ebony', 'Fetish', 'Fisting', 'Gay', 'Groupsex', 'Handjob', 'Hardcore', 'Hentai', 'Homemade', 'Indian', 'Interracial', 'Japanese', 'Latina', 'Lesbian', 'Massage', 'Masturbation', 'Mature', 'MILF', 'POV', 'Public', 'Redhead', 'Shemale', 'Small Tits', 'Solo', 'Squirt', 'Striptease', 'Teen', 'Threesome', 'Toy', 'Vintage', 'VR'];

const Category = styled(NavLink)`
  transition-duration: 0.2s;
  background-color: ${props => props.theme.contentColor};
  font-size: 1.25rem;
  text-align: center;
  padding: 0.5rem 1rem;
  border-radius: 4px;
  overflow: hidden;
  box-shadow: 0 1px 6px rgba(0,0,0,0.08), 0 3px 10px rgba(0,0,0,0.08);
  cursor: pointer;
  text-decoration: none;
  color: ${props => props.theme.textColor};
  &:hover {
    transition-duration: 0.2s;
    color: ${props => props.theme.accentColor};
  }
`;

const Grid = styled.div`
  display: grid;
  width: 100%;
  margin: 0 auto;
  grid-auto-flow: row dense;
  grid-template-columns: repeat(auto-fill, 150px);
  grid-gap: 0.5rem;
  ${media.md`
    grid-gap: 1rem;
  `}
  justify-content: center;
`;

const Categories = ({ theme }) => {
  document.title = `${theme.name} - Categories`;
  const categoryList = categories.map(category => <Category to={`/s/${category}/0`}>{category}</Category>);
  return (
    <Container style={{ padding: '1rem 0 0 0' }}>
      <Grid style={{ marginBottom: '2rem' }}>
        {categoryList}
      </Grid>
      <Media query="(min-width: 915px)">
        {matches => (matches ? (<Ad type="small-banner" />) : (<Ad type="mobile-small-banner" />))}
      </Media>
    </Container>
  );
};

export default withTheme(Categories);
