import React from 'react';
import styled from 'styled-components';
import Container from '/components/Container/Container';
import VideoGrid from '/components/VideoGrid/VideoGrid';
import Title from '/components/Title/Title';
import Ad from '/components/Ad/Ad';
import { client } from '/services/es';

const Wrapper = styled(Container)`
  display: grid;
  padding-top: 3rem;
  grid-template-columns: 1fr 1fr;
  grid-template-areas: "text video"
                        "ad video";
  grid-gap: 16px;
  justify-items: right;
`;

const TextWrapper = styled.div`
  color: ${props => props.theme.textColor};
  width: fit-content;
  text-align: right;
  grid-area: text;
  p {
    margin-bottom: 0;
  }
`;

class PromotedContent extends React.Component {
  constructor(props) {
    super(props);
    this.date = new Date();
    this.state = {
      videos: [],
    };
  }

  componentDidMount() {
    const videosQuery = {
      size: 1,
      query: {
        function_score: {
          query: {
            range: {
              plays: {
                gte: '1000',
              },
            },
          },
          functions: [
            {
              random_score: {
                seed: this.date.getDate + this.date.getMonth + this.date.getFullYear,
              },
            },
          ],
        },
      },
    };

    client.get(videosQuery)
      .then((response) => {
        this.setState({ videos: response.data.hits.hits });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    return (
      <Wrapper>
        <TextWrapper>
          <Title>Content of the day</Title>
          <p>Check out our daily promoted content</p>
        </TextWrapper>
        <VideoGrid videos={this.state.videos} total={1} style={{ gridArea: 'video' }} />
        <Ad type="promoted" style={{ marginTop: '-16px' }} style={{ gridArea: 'ad' }} />
      </Wrapper>
    );
  }
}

export default PromotedContent;
