import React, { Component } from 'react';
import styled, { withTheme } from 'styled-components';
import seedrandom from 'seedrandom';
import { client } from '/services/es';
import Container from '/components/Container/Container';
import VideoGrid from '/components/VideoGrid/VideoGrid';
import Title from '/components/Title/Title';
import Ad from '/components/Ad/Ad';
import media from '/services/style-utils';
// import PromotedContent from './components/PromotedContent/PromotedContent';
import Media from 'react-media';

const categories = ['Milf', 'Big tits', 'Big ass', 'Teen', 'Creampie', 'Lesbian', 'Anal'];

const PromotedVideos = styled(Container)`
  display: grid;
  grid-template-columns: 1fr;
  grid-template-areas: "videos"
                        "ad";
  ${media.sm`
    grid-template-columns: ${props => ((props.length - 1) * 300) + ((props.length - 2) * 16)}px auto;
    grid-template-areas: "videos ad";
  `}
  grid-gap: 16px;
  justify-content: center;
`;

class Homepage extends Component {
  constructor(props) {
    super(props);
    document.title = `${this.props.theme.name} - Free porn videos`;
    this.date = new Date();
    this.handleResize = this.handleResize.bind(this);
    this.state = {
      topVideos: [],
      topVideosInCategory: [],
      category: categories[Math.floor(Math.random() * categories.length)],
      videosPerRow: Math.min(Math.max(Math.floor(window.innerWidth / 300), 2), 5),
    };
  }

  componentDidMount() {
    const topVideosQuery = {
      query: {
        bool: {
          must: {
            range: {
              uploaded: {
                lte: 'now/d',
                gte: 'now-1d/d',
              },
            },
          },
          must_not: {
            terms: {
              tags: ['gay', 'shemale'],
            },
          },
        },
      },
      sort: [
        {
          uploaded: {
            order: 'desc',
          },
        },
        {
          plays: {
            order: 'desc',
          },
        },
      ],
      size: (this.state.videosPerRow - 1) * 3,
    };

    const topVideosInCategoryQuery = {
      query: {
        bool: {
          must: {
            range: {
              uploaded: {
                lte: 'now/d',
                gte: 'now-6d/d',
              },
            },
          },
          should: {
            match: {
              tags: this.state.category,
            },
          },
          must_not: {
            terms: {
              tags: ['gay', 'shemale'],
            },
          },
        },
      },
      sort: {
        plays: {
          order: 'desc',
        },
      },
      size: this.state.videosPerRow * 3,
    };

    client.get(topVideosQuery)
      .then((response) => {
        this.setState({ topVideos: response.data.hits.hits });
      })
      .catch((error) => {
        console.log(error);
      });

    client.get(topVideosInCategoryQuery)
      .then((response) => {
        this.setState({ topVideosInCategory: response.data.hits.hits });
      })
      .catch((error) => {
        console.log(error);
      });

    window.addEventListener('resize', this.handleResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }

  handleResize() {
    this.setState({
      videosPerRow: Math.min(Math.max(Math.floor(window.innerWidth / 316), 2), 5),
    });
  }

  render() {
    return (
      <React.Fragment>
        <Container>
          <Title>Top Videos Today</Title>
        </Container>
        <PromotedVideos fluid length={this.state.videosPerRow} style={{ marginBottom: '2rem' }}>
          <VideoGrid videos={this.state.topVideos} noLazyLoad withAd total={(this.state.videosPerRow - 1) * 3} style={{ gridArea: 'videos' }} />
          <Ad type="promoted-videos-homepage" style={{ gridArea: 'ad' }} />
        </PromotedVideos>
        {/* <PromotedContent /> */}
        <Media query="(min-width: 915px)">
          {matches => (matches ? (<Ad type="small-banner" />) : (<Ad type="mobile-small-banner" />))}
        </Media>
        <Container style={{ marginTop: '2rem' }}>
          <Title>{`Top Videos In ${this.state.category}`}</Title>
        </Container>
        <Container fluid style={{ marginBottom: '2rem' }}>
          <VideoGrid videos={this.state.topVideosInCategory} total={this.state.videosPerRow * 3} />
        </Container>
        <Media query="(min-width: 915px)">
          {matches => (matches ? (<Ad type="big-banner" />) : (<Ad type="mobile-big-banner" />))}
        </Media>
      </React.Fragment>
    );
  }
}

export default withTheme(Homepage);
