import React, { Component } from 'react';
import styled, { withTheme } from 'styled-components';

import media from '/services/style-utils';
import { client } from '/services/es';
import Container from '/components/Container/Container';
import Video from '/components/Video/Video';
import VideoGrid from '/components/VideoGrid/VideoGrid';
import Ad from '/components/Ad/Ad';
import Media from 'react-media';
import Title from '/components/Title/Title';

const StyledContainer = styled(Container)`
  display: grid;
  margin-top: 8px;
  grid-template-columns: auto 300px;
  grid-auto-rows: auto;
  grid-gap: 16px;
  grid-template-areas: "video video"
                        "ad ad"
                        "title title";
  ${media.lg`
    margin-top: 0px;
    grid-template-areas: "video ad"
                          "title title";
  `}
`;

class Watch extends Component {
  constructor(props) {
    super(props);
    document.title = `${this.props.theme.name} - Video`;
    this.state = {
      video: undefined,
      videosPerRow: Math.min(Math.max(Math.floor(window.innerWidth / 300), 2), 5),
      simVideos: [],
      iframeKey: 0,
    };
  }

  componentDidMount() {
    window.scrollTo(0, 0);

    const videoQuery =
    this.props.match.params.id !== 'random' ? {
      query: {
        match: {
          _id: this.props.match.params.id,
        },
      },
    } : {
      size: 1,
      query: {
        function_score: {
          functions: [
            {
              random_score: {
                seed: new Date().valueOf(),
              },
            },
          ],
        },
      },
    };

    client.get(videoQuery)
      .then((response) => {
        if (response.data.hits.total > 0) {
          document.title = `${this.props.theme.name} - ${response.data.hits.hits[0]._source.title}`;
          this.setState({ video: response.data.hits.hits[0]._source });
        } else {
          this.props.history.push('/v/random');
        }
      });

    if (!document.getElementById('popunder')) {
      const popunderConfigScript = document.createElement('script');
      popunderConfigScript.type = 'text/javascript';
      popunderConfigScript.innerHTML = 'var ad_idzone = "3060140",ad_frequency_period = 180,ad_frequency_count = 1,ad_trigger_method = 1;';

      const popunderScript = document.createElement('script');
      popunderScript.type = 'text/javascript';
      popunderScript.id = 'popunder';
      popunderScript.src = 'https://ads.exosrv.com/popunder1000.js';
      document.body.appendChild(popunderConfigScript);
      document.body.appendChild(popunderScript);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const shouldRefresh = this.props.location.key !== prevProps.location.key;
    if (this.props.match.params.id !== prevProps.match.params.id || shouldRefresh) {
      this.setState(state => ({ iframeKey: state.iframeKey + 1 }));
      const videoQuery =
        this.props.match.params.id !== 'random' ? {
          query: {
            match: {
              _id: this.props.match.params.id,
            },
          },
        } : {
          size: 1,
          query: {
            function_score: {
              functions: [
                {
                  random_score: {
                    seed: new Date().valueOf(),
                  },
                },
              ],
            },
          },
        };

      client.get(videoQuery)
        .then((response) => {
          document.title = `${this.props.theme.name} - ${response.data.hits.hits[0]._source.title}`;
          this.setState({ video: response.data.hits.hits[0]._source });
          window.scrollTo(0, 0);
        });
    }

    if (this.state.video !== prevState.video) {
      const tagTerms = [];
      this.setState({ simVideos: [] });
      this.state.video.tags.forEach((tag) => {
        tagTerms.push({ term: { tags: tag } });
      });

      const simVideosQuery = {
        query: {
          bool: {
            should: tagTerms,
            must_not: [{
              term: {
                id: this.state.video.id,
              },
            },
            {
              terms: {
                tags: ['gay', 'shemale'],
              },
            },
            ],
            minimum_should_match: '2<-75%',
          },
        },
        size: this.state.videosPerRow * 4,
      };

      client.get(simVideosQuery)
        .then((response) => {
          this.setState({ simVideos: response.data.hits.hits });
        });
    }
  }

  render() {
    return (
      <React.Fragment>
        <StyledContainer style={{ padding: '0 2rem' }}>
          <Video style={{ gridArea: 'video' }} video={this.state.video} />
          <Media query="(min-width: 992px)">
            {matches =>
              (matches ? (<Ad style={{ gridArea: 'ad' }} type="dual-square" key={this.state.iframeKey} />) :
              (<Ad style={{ gridArea: 'ad' }} type="square" key={this.state.iframeKey} />))
            }
          </Media>
          <Title style={{ gridArea: 'title' }}>Similiar videos</Title>
        </StyledContainer>
        <Container fluid style={{ marginBottom: '2rem' }}>
          <VideoGrid videos={this.state.simVideos} noLazyLoad total={this.state.videosPerRow * 4} />
        </Container>
        <Media query="(min-width: 915px)">
          {matches => (matches ? (<Ad type="small-banner" key={this.state.iframeKey + 1} />) : (<Ad type="mobile-small-banner" key={this.state.iframeKey + 1} />))}
        </Media>
      </React.Fragment>
    );
  }
}

export default withTheme(Watch);
