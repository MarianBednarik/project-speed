import React, { Component } from 'react';

import { client } from '/services/es';
import Title from '/components/Title/Title';
import Container from '/components/Container/Container';
import VideoGrid from '/components/VideoGrid/VideoGrid';
import Paginator from '/components/Paginator/Paginator';
import { withTheme } from 'styled-components';
import Ad from '/components/Ad/Ad';
import Media from 'react-media';

class New extends Component {
  constructor(props) {
    super(props);
    this.state = {
      videos: [],
      videosPerRow: Math.min(Math.max(Math.floor(window.innerWidth / 308), 2), 5),
      lastPage: 0,
      iframeKey: 0,
    };
  }

  componentDidMount() {
    document.title = `${this.props.theme.name} - New`;
    this.fetchResults();
  }

  componentDidUpdate(prevProps) {
    const doesPageMatch = this.props.match.params.page === prevProps.match.params.page;
    if (!doesPageMatch) {
      this.fetchResults();
    }
  }

  fetchResults() {
    this.setState(state => ({ videos: [], iframeKey: state.iframeKey + 1 }));
    const query = {
      query: {
        match_all: {},
      },
      sort: {
        uploaded: {
          order: 'desc',
        },
      },
      from: Number(this.props.match.params.page) * this.state.videosPerRow * 6,
      size: this.state.videosPerRow * 6,
    };

    client.get(query)
      .then((response) => {
        this.setState({
          videos: response.data.hits.hits,
          lastPage: Math.floor(response.data.hits.total / (this.state.videosPerRow * 6)),
        });
      });
  }

  render() {
    return (
      <React.Fragment>
        <Container>
          <Title>New videos</Title>
        </Container>
        <Container fluid style={{ marginBottom: '2rem' }}>
          <VideoGrid videos={this.state.videos} noLazyLoad total={this.state.videosPerRow * 6} />
          <Paginator currentPage={Number(this.props.match.params.page)} lastPage={this.state.lastPage} />
        </Container>
        <Media query="(min-width: 915px)">
          {matches => (matches ? (<Ad type="small-banner" key={this.state.iframeKey} />) : (<Ad type="mobile-small-banner" key={this.state.iframeKey} />))}
        </Media>
      </React.Fragment>
    );
  }
}

export default withTheme(New);
