import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Media from 'react-media';
import { client } from '/services/es';
import Ad from '/components/Ad/Ad';
import Title from '/components/Title/Title';
import Container from '/components/Container/Container';
import VideoGrid from '/components/VideoGrid/VideoGrid';
import Paginator from '/components/Paginator/Paginator';
import { withTheme } from '../../../node_modules/styled-components';

class Search extends Component {
  constructor(props) {
    super(props);
    document.title = `${this.props.theme.name} - Search`;
    this.state = {
      videos: [],
      videosPerRow: Math.min(Math.max(Math.floor(window.innerWidth / 300), 2), 5),
      lastPage: 0,
      iframeKey: 0,
    };
    this.categories = ['amateur', 'anal', 'asian', 'babe', 'bbw', 'big ass', 'big dick', 'big tits', 'blonde', 'blowjob', 'bondage', 'brunette', 'cam', 'compilation', 'creampie', 'cumshot', 'deep throat', 'dp', 'ebony', 'fetish', 'fisting', 'gay', 'groupsex', 'handjob', 'hardcore', 'hentai', 'homemade', 'indian', 'interracial', 'japanese', 'latina', 'lesbian', 'massage', 'masturbation', 'mature', 'milf', 'pov', 'public', 'redhead', 'shemale', 'small tits', 'solo', 'squirt', 'striptease', 'teen', 'threesome', 'toy', 'vintage', 'vr'];
    this.fetchResults();
  }

  componentDidUpdate(prevProps) {
    const doesTermMatch = this.props.match.params.term === prevProps.match.params.term;
    const doesPageMatch = this.props.match.params.page === prevProps.match.params.page;
    if (!doesPageMatch || !doesTermMatch) {
      this.fetchResults();
    }
  }

  fetchResults() {
    if (this.state.videos.length > 0) {
      this.setState(state => ({ iframeKey: state.iframeKey + 1, videos: [] }));
    }
    const query = this.categories.includes(this.props.match.params.term.toLowerCase()) ? {
      query: {
        bool: {
          must: [
            {
              term: {
                tags: this.props.match.params.term,
              },
            }, {
              range: {
                uploaded: {
                  lte: 'now/d',
                  gte: 'now-6d/d',
                },
              },
            },
          ],
        },
      },
      sort: {
        plays: {
          order: 'desc',
        },
      },
      from: Number(this.props.match.params.page) * this.state.videosPerRow * 6,
      size: this.state.videosPerRow * 6,
    } : {
      query: {
        match: {
          title: this.props.match.params.term,
        },
      },
      sort: [
        {
          plays: {
            order: 'desc',
          },
        },
      ],
      track_scores: true,
      from: Number(this.props.match.params.page) * this.state.videosPerRow * 6,
      size: this.state.videosPerRow * 6,
    };

    client.get(query)
      .then((response) => {
        document.title = `${this.props.theme.name} - Search: ${this.props.match.params.term}`;
        this.setState({
          videos: response.data.hits.hits,
          lastPage: Math.floor(response.data.hits.total / (this.state.videosPerRow * 6)),
        });
      });
  }

  render() {
    return (
      <React.Fragment>
        <Container>
          <Title>{`${this.state.videos ? (this.state.videos.length > 0 ? 'Search' : 'No search') : 'Search'} results for "${this.props.match.params.term}"`}</Title>
        </Container>
        <Container fluid>
          <VideoGrid videos={this.state.videos} noLazyLoad total={this.state.videosPerRow * 6} />
          <Paginator currentPage={Number(this.props.match.params.page)} lastPage={this.state.lastPage} searchTerm={this.props.match.params.term} style={{ marginBottom: '2rem' }} />
          <Media query="(min-width: 915px)">
            {matches => (matches ? (<Ad type="big-banner" key={this.state.iframeKey} />) : (<Ad type="mobile-big-banner" key={this.state.iframeKey} />))}
          </Media>
        </Container>
      </React.Fragment>
    );
  }
}

export default withTheme(Search);
