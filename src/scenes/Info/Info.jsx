import React from 'react';
import Container from '/components/Container/Container';
import Title from '/components/Title/Title';
import styled from 'styled-components';

const Paragraph = styled.p`
  color: ${props => props.theme.textColor};
  font-size: 1.2rem;
  margin: 0;
`;

const Info = () => (
  <Container style={{ paddingTop: '1rem' }}>
    <Title>About us</Title>
    <Paragraph>
      Reporned is all about aggragating the best of porn into one place.
      Find all of your favorite videos here, or discover new videos (they are updated daily!).
    </Paragraph>
    <Title>Legal</Title>
    <Paragraph>
      Reporned does not own, host or is affiliated with the content displayed on this site.
      We do not produce these vidoes and are dependand on external sources, which are out of our control.
      We only agregate embeded videos from other sites. Any complaints or DMCA takedowns
      need to be forwarded to the respected video owners, however, we can stop a video from showing up on our site.
    </Paragraph>
    <br />
    <Paragraph>
      If you would like a video to be removed, please contact us thought the contact information provided below. We can only stop a video from being indexed
      by our system, but not completely remove it. You will need to contact its respective owner for that.
    </Paragraph>
    <br />
    <Paragraph>
      We limit the use of our site to adults over 18 years of age or the age of majority in the individual’s jurisdiction,
      whichever is greater. Anyone under this age is strictly forbidden from using this site.
    </Paragraph>
    <Title>Data collection</Title>
    <Paragraph>
      All of the data collected is anonymized to avoid fingerprinting. By using our site, only your IP address,
      country of origin and other non-personal information about your computer or device (such as web requests, browser type, browser language, referring URL,
      operating system and date and time of requests) will be recorded for log file information. This information will only be used for analytics and performance
      monitoring pusposes.
    </Paragraph>
  </Container>
);

export default Info;
