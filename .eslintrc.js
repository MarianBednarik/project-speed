module.exports = {
  "extends": "airbnb",
  "env": {
    "browser": true,
    "node": true
  },
  "rules": {
    "import/no-unresolved": false,
    "import/extensions": false,
    "import/no-absolute-path": false,
    "no-underscore-dangle": 0,
  }   
};